# G2 Integration - Drupal 9

## Introduction

![G2](g2.png)

The G2 System implementation. Contains the following modules:
* G2 API
* G2 Reviews Block
* G2 Reviews Field
* G2 Review Form

## Requirements

* [Token](https://www.drupal.org/project/token)

## Installation

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

## Configuration

* Configure the user permissions in `Administration » People » Permissions`.
* Configure credentials in `Administration » Configuration » System » G2 - Configurations`.

## Troubleshooting

* If there is no result for a particular product ID:
  - Go under `Administration » Configuration » System » G2 - Configurations`
  - Make sure that `Debug` mode is turned on. If not, enable it
  - Perform a test request
  - Go under `Administration » Reports` and see logs for monitoring
