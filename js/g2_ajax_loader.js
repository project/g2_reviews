/**
 * @file
 * G2 reviews ajax loader element.
 */

(function ($) {

  /**
   * The ajax loader element.
   */
  var ajaxLoader = $('\
    <div class="g2-ajax-loader">\
      <div class="g2-ajax-loader__inner">\
        <div class="g2-ajax-loader__content"><span class="spinner"></span></div>\
      </div>\
    </div>');

  /**
   * Add a custom trigger when beforeSend fires.
   */
  Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    if (options.url) {
      var urlParts = options.url.split('/');
      if (urlParts[1] == 'g2-reviews-widget') {
        // Firstly, find the ajax loader element and remove it. Then, attach the
        // loader to the body selector.
        $('body').find('.g2-ajax-loader').remove();
        $('body').prepend(ajaxLoader);
      }
    }

    if (this.$form) {
      options.extraData = options.extraData || {};
      options.extraData.ajax_iframe_upload = '1';
      var v = $.fieldValue(this.element);

      if (v !== null) {
        options.extraData[this.element.name] = v;
      }
    }
    $(this.element).prop('disabled', true);

    if (!this.progress || !this.progress.type) {
      return;
    }

    var progressIndicatorMethod = "setProgressIndicator".concat(this.progress.type.slice(0, 1).toUpperCase()).concat(this.progress.type.slice(1).toLowerCase());

    if (progressIndicatorMethod in this && typeof this[progressIndicatorMethod] === 'function') {
      this[progressIndicatorMethod].call(this);
    }
  }

  /**
   * Remove ajax loader once the ajax request was performed.
   */
  $(document).ajaxComplete(function () {
    $('body').find('.g2-ajax-loader').remove();
  });

  $(document).ajaxError(function () {
    $('body').find('.g2-ajax-loader').remove();
  });

})(jQuery);

