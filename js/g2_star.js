/**
 * @file
 * G2 reviews star js logic.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * A product reviews star plugin.
   */
  Drupal.behaviors.g2ReviewsStar = {
    attach: function (context, settings) {
      // Configure and init star rating.
      $('.g2-star-rating-icons').starRating({
        readOnly: true,
        totalStars: 5,
        emptyColor: 'white',
        starGradient: {
          start: '#ff492c',
          end: '#ff492c'
        },
        starSize: 21
      });
    }
  };

})(jQuery, Drupal, drupalSettings);

