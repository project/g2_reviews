# The G2 Review Form module

## Introduction

The G2 Reviews Form module is the product review form submit implementations.

## Troubleshooting

* If there is no result for a particular product ID:
  - Go under `Administration » Configuration » System » G2 - Configurations`
  - Make sure that `Debug` mode is turned on. If not, enable it
  - Perform a test request
  - Go under `Administration » Reports` and see logs for monitoring

## FAQ

Q: I enabled the module. How do I use it?

A:
In order to place a block you need to perform the following steps:
* Go under `Administration » Structure » Block layout`
* Click on `Place a block` under a corresponding region and find the Category `G2`
* Place a `G2 Product Review - Form Builder` block and configure it by setting the
product ID value (make sure the ID is existent otherwise it won't draw the filters configuration form)
* Once you set a valid product ID you can generate the iframe review form.

Q: I did all the steps for placing a review submit form via iframe. Why do I get
the message `First name should be set in profile to submit review form`?

A:
In order to see the review form and do not see that message you need to set a
first and last name for the user. Also, make sure you set the partner name under
`Administration » Configuration » System » G2 - Configurations` page.

Q: I've done all mentioned steps above and I submitted a review but I can't see it. Why?

A:
In order to view a newly placed review it must be moderated by the `G2` moderators first.
Once the review will be approved, it will appear on the list of reviews for that
particular product you submitted for.
