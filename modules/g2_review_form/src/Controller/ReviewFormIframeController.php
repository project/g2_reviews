<?php

declare(strict_types = 1);

namespace Drupal\g2_review_form\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\g2_reviews\Services\G2ReviewIframe;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ReviewFormIframeController.
 *
 * @package Drupal\g2_review_form\Controller
 */
class ReviewFormIframeController extends ControllerBase {

  /**
   * The G2 Iframe service.
   *
   * @var \Drupal\g2_reviews\Services\G2ReviewIframe
   */
  protected G2ReviewIframe $g2ReviewIframe;

  /**
   * ReviewFormIframeController constructor.
   *
   * @param \Drupal\g2_reviews\Services\G2ReviewIframe $g2ReviewIframe
   *   The G2 Iframe service.
   */
  public function __construct(G2ReviewIframe $g2ReviewIframe) {
    $this->g2ReviewIframe = $g2ReviewIframe;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('g2_reviews.review_iframe')
    );
  }

  /**
   * Build response object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function iframeResponse(Request $request): Response {

    $response = new Response();

    $pId = $request->query->get('pid');
    if (!$pId) {
      $response->setContent($this->t('Product ID is required'));
      return $response;
    }

    try {
      $markup = $this->g2ReviewIframe->iframeData($pId)['iframe'];
    }
    catch (\Exception $e) {
      $markup = $e->getMessage();
    }

    $response->headers->set('Content-Type', 'text/html; charset=utf-8');
    $response->setContent($markup);

    return $response;
  }

}
