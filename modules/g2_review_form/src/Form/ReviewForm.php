<?php

declare(strict_types = 1);

namespace Drupal\g2_review_form\Form;

use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\g2_reviews\Exception\InsufficientUserDataException;
use Drupal\g2_review_form\Services\G2FormProcessingService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ReviewForm.
 *
 * @package Drupal\g2_review_form\Form
 */
class ReviewForm extends FormBase {

  /**
   * Form build service.
   *
   * @var \Drupal\g2_review_form\Services\G2FormProcessingService
   */
  private G2FormProcessingService $g2FormBuilderService;

  /**
   * ReviewForm constructor.
   *
   * @param \Drupal\g2_review_form\Services\G2FormProcessingService $g2FormBuilderService
   *   G2 Form builder service.
   */
  public function __construct(G2FormProcessingService $g2FormBuilderService) {
    $this->g2FormBuilderService = $g2FormBuilderService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('g2_review_form.form_processing_service')
    );
  }

  /**
   * Form id.
   *
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'g2_review_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $productId = $this->getRequest()->query->get('pid');
    if (!$productId) {
      $this->messenger()->addError($this->t('Product ID is required'));
      return $form;
    }

    try {
      $this->g2FormBuilderService->populateDrupalFormWithG2Fields($form, $productId);
    }
    catch (InsufficientUserDataException | ConfigValueException $exception) {
      $this->messenger()->addError($exception->getMessage());
      return $form;
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit review'),
      '#button_type' => 'primary',
      '#weight' => 2000,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $submittedData = $form_state->getValues();
    $formErrors = $this->g2FormBuilderService->validateInputAccordingG2Validation($form, $submittedData);

    if (!empty($formErrors)) {
      foreach ($formErrors as $fieldName => $formError) {
        $form_state->setErrorByName($fieldName, $formError);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $submittedData = $form_state->getValues();

    $apiResponse = $this->g2FormBuilderService->submitReviewFormToG2($submittedData);

    if ($apiResponse) {
      $this->messenger()->addStatus($this->t('Review submitted.'));
    }
    else {
      $this->messenger()->addError($this->t('Something went wrong.'));
    }
  }

}
