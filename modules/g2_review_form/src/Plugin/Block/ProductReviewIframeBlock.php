<?php

declare(strict_types = 1);

namespace Drupal\g2_review_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\g2_reviews\Services\G2ReviewIframe;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a product review iframe form.
 *
 * @Block(
 *   id = "g2_product_review_form_iframe",
 *   admin_label = @Translation("G2 Product Review Form - Iframe"),
 *   category = @Translation("G2"),
 * )
 */
class ProductReviewIframeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The G2 Iframe service.
   *
   * @var \Drupal\g2_reviews\Services\G2ReviewIframe
   */
  protected G2ReviewIframe $g2ReviewIframe;

  /**
   * The constructor for ProductReviewIframeBlock block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\g2_reviews\Services\G2ReviewIframe $g2ReviewIframe
   *   The G2 Iframe service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, G2ReviewIframe $g2ReviewIframe) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->g2ReviewIframe = $g2ReviewIframe;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('g2_reviews.review_iframe')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $form = parent::blockForm($form, $form_state);

    $form['product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#description' => $this->t('The G2 product ID to work with'),
      '#default_value' => !empty($config['product_id']) ? $config['product_id'] : '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getUserInput();
    $this->configuration['product_id'] = $values['settings']['product_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->getConfiguration();
    $data = $this->g2ReviewIframe->iframeData($config['product_id']);
    return [
      '#type' => 'inline_template',
      '#template' => $data['iframe'],
      '#context' => [
        'url' => $data['domain'],
      ],
    ];
  }

}
