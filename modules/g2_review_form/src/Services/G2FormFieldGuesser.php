<?php

declare(strict_types = 1);

namespace Drupal\g2_review_form\Services;

/**
 * Class G2FormFieldGuesser.
 *
 * @package Drupal\g2_review_form\Services
 */
class G2FormFieldGuesser {

  /**
   * Guess Drupal form field based on G2 question info.
   *
   * @param array $g2Field
   *   G2 field.
   *
   * @return array|string[]
   *   Mapped Drupal field.
   */
  public function prepareDrupalFormFieldBasedOnG2Question(array $g2Field): array {
    $field = [
      '#title' => $g2Field['name'],
      '#required' => $this->isFieldRequired($g2Field),
    ];

    $minCharactersCount = $g2Field["validations"]["min_character_count"];
    if ($minCharactersCount !== NULL) {
      $field['#description'] = $this->getMinCountDescription($minCharactersCount);
      $field['#size'] = $minCharactersCount;
    }

    switch ($g2Field['input_type']) {
      case 'radio':
        $field += [
          '#type' => 'radios',
          '#options' => $this->prepareOptionsFromG2Field($g2Field['options']),
        ];
        break;

      case 'select':
        $field += [
          '#type' => 'select',
          '#options' => $this->prepareOptionsFromG2Field($g2Field['options']),
        ];
        break;

      case 'multiselect':
        $field += [
          '#type' => 'checkboxes',
          '#options' => $this->prepareOptionsFromG2Field($g2Field['options']),
        ];
        break;

      case 'checkbox':
        $field += [
          '#type' => 'checkbox',
        ];
        break;

      case 'short_text':
        $field += [
          '#type' => 'textfield',
        ];
        break;

      case 'text':
        $field += [
          '#type' => 'textarea',
        ];
        break;
    }

    return $field;
  }

  /**
   * Method which returns description for fields with min count validation.
   *
   * @param int $minCount
   *   Min count.
   *
   * @return string
   *   Description for field with Min count validation.
   */
  private function getMinCountDescription(int $minCount): string {
    return 'Minimum characters ' . $minCount;
  }

  /**
   * Determine if G2 field is required.
   *
   * @param array $g2Field
   *   G2 field.
   *
   * @return bool
   *   Field is required or not.
   */
  private function isFieldRequired(array $g2Field): bool {
    // Make withhold_name field as unrequired, user will have possibility
    // to decide hide his name or not.
    if ($g2Field["id"] === 'withhold_name') {
      return FALSE;
    }

    return $g2Field['validations']['required'];
  }

  /**
   * Prepare options for Drupal select, radios, checkboxes... fields.
   *
   * @param array $g2FieldOptions
   *   G2 field options.
   *
   * @return array
   *   Options mapped to correspond Drupal options.
   */
  private function prepareOptionsFromG2Field(array $g2FieldOptions): array {
    $options = [];
    foreach ($g2FieldOptions as $g2FieldOption) {
      $options[$g2FieldOption['option_id']] = (string) $g2FieldOption['text'];
    }

    return $options;
  }

}
