<?php

declare(strict_types = 1);

namespace Drupal\g2_review_form\Services;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\g2_reviews\Services\G2Common;
use Drupal\g2_reviews\Services\G2;

/**
 * Class FormBuilder.
 *
 * @package Drupal\g2_review_form\Services
 */
class G2FormProcessingService {

  public const FORM_FIELD_PREFIX = 'g2_form_field_';

  use StringTranslationTrait;

  /**
   * G2 form fields array.
   *
   * @var array
   */
  private array $g2FormFields;

  /**
   * The G2 API service.
   *
   * @var \Drupal\g2_reviews\Services\G2
   */
  private G2 $g2;

  /**
   * The Form field guesser.
   *
   * @var \Drupal\g2_review_form\Services\G2FormFieldGuesser
   */
  private G2FormFieldGuesser $formFieldGuesser;

  /**
   * G2 Common service.
   *
   * @var \Drupal\g2_reviews\Services\G2Common
   */
  private G2Common $g2CommonService;

  /**
   * G2FormBuilderService constructor.
   *
   * @param \Drupal\g2_reviews\Services\G2 $g2
   *   G2 API service.
   * @param \Drupal\g2_review_form\Services\G2FormFieldGuesser $formFieldGuesser
   *   G2 field guesser service.
   * @param \Drupal\g2_reviews\Services\G2Common $g2CommonService
   *   G2 common service.
   */
  public function __construct(G2 $g2, G2FormFieldGuesser $formFieldGuesser, G2Common $g2CommonService) {
    $this->g2 = $g2;
    $this->formFieldGuesser = $formFieldGuesser;
    $this->g2CommonService = $g2CommonService;
  }

  /**
   * Populate Drupal form with fields received from API.
   *
   * @param array $form
   *   Drupal form.
   * @param string $productId
   *   Product ID for which G2 form should be loaded.
   *
   * @throws \Drupal\g2_reviews\Exception\InsufficientUserDataException
   *    Throw if user not logged in or doesn't have first or last name.
   */
  public function populateDrupalFormWithG2Fields(array &$form, string $productId): void {
    $userData = $this->g2CommonService->getUserData();
    $this->g2FormFields = $this->getG2FormData($productId, $userData['email']);

    $this->addQuestionsFormFields($form);
    $this->addTermsAndConditionsFormField($form);
    $this->addProductIdFormField($form, $productId);
  }

  /**
   * Get G2 form fields from API.
   */
  private function getG2FormData(string $productId, string $userEmail): array {
    return $this->g2->getProductEmbeddedtReviewForm($productId, $userEmail);
  }

  /**
   * Add terms and condition checkbox to review form.
   *
   * @param array $form
   *   Form to populate.
   */
  private function addTermsAndConditionsFormField(array &$form): void {
    $termLinks = $this->g2FormFields["data"]["attributes"]["terms_accepted"]["links"];
    $termsDescription =
      "I accept <a href='{$termLinks["terms_of_use"]}' target='_blank'>terms of use</a> " .
      "and <a href='{$termLinks["privacy_policy"]}' target='_blank'>privacy policy</a>.";

    $form[self::FORM_FIELD_PREFIX . 'terms_and_conditions'] = [
      '#type' => 'checkbox',
      '#title' => $termsDescription,
      '#required' => TRUE,
      '#weight' => 500,
    ];
  }

  /**
   * Helper method which adds G2 product Id to Drupal form.
   *
   * @param array $form
   *   Form array.
   * @param string $productId
   *   G2 product Id.
   */
  private function addProductIdFormField(array &$form, string $productId): void {
    $form[self::FORM_FIELD_PREFIX . 'product_id'] = [
      '#type' => 'hidden',
      '#value' => $productId,
    ];
  }

  /**
   * Add questions to Drupal form based on data from API.
   *
   * @param array $form
   *   Drupal form.
   */
  private function addQuestionsFormFields(array &$form): void {
    $questionFields = $this->g2FormFields['data']['attributes']['relationships']['questions']['data'];
    foreach ($questionFields as $key => $questionField) {
      $fieldName = self::FORM_FIELD_PREFIX . $questionField['id'];
      $form[$fieldName] = $this->formFieldGuesser->prepareDrupalFormFieldBasedOnG2Question($questionField);
      $form[$fieldName]['#weight'] = $key;
    }
  }

  /**
   * Validate user form input according to G2 filed validation.
   *
   * @param array $form
   *   Drupal form.
   * @param array $input
   *   User input.
   *
   * @return array
   *   With validation errors if any.
   */
  public function validateInputAccordingG2Validation(array $form, array $input): array {
    $validationErrors = [];

    $g2Fields = $this->getG2FormFields($form);
    foreach ($g2Fields as $fieldName => $field) {
      if ($field['#required'] && empty($input[$fieldName])) {
        $validationErrors[$fieldName] = $this->t('Field @fieldName is required', ['fieldName' => $field['#title']]);
      }

      $countable = $field['#size'] ?? NULL;
      if ($countable && $field['#type'] === 'textarea' && strlen($input[$fieldName]) < $countable) {
        $validationErrors[$fieldName] = $this->t(
          'In field @fieldName should be entered minimum @symbolsCount characters.',
          ['@fieldName' => $field['#title'], '@symbolsCount' => $countable]
        );
      }
    }

    return $validationErrors;
  }

  /**
   * Prepare data and submit to G2 API.
   *
   * @param array $submittedValues
   *   Form submitted values.
   *
   * @return bool
   *   Status of the form submission.
   *
   * @throws \Drupal\g2_reviews\Exception\InsufficientUserDataException
   *   Exception.
   */
  public function submitReviewFormToG2(array $submittedValues): bool {
    $g2FormFields = $this->getG2FormFields($submittedValues);
    $g2FormFields = $this->removePrefixFromFieldNames($g2FormFields);

    $preparedData = $this->prepareFormValuesForSubmit($g2FormFields);

    try {
      $response = $this->g2->submitProductReview($preparedData);
      return isset($response['status']) && $response['status'] === 'success';
    }
    catch (\Exception $exception) {
      return FALSE;
    }
  }

  /**
   * Get G2 form fields from submitted data.
   *
   * @param array $formData
   *   Form submitted values.
   *
   * @return array
   *   Form fields without default drupal submitted data.
   */
  private function getG2FormFields(array $formData): array {
    return array_filter($formData, function ($fieldName) {
      return (strpos($fieldName, self::FORM_FIELD_PREFIX) === 0);
    }, ARRAY_FILTER_USE_KEY);
  }

  /**
   * Remove prefixes from field names.
   *
   * @param array $submittedValues
   *   Form submitted values.
   *
   * @return array
   *   Form fields with proper field names.
   */
  private function removePrefixFromFieldNames(array $submittedValues): array {
    $valuesWithProperFieldNames = [];
    foreach ($submittedValues as $fieldName => $submittedValue) {
      $fieldId = str_replace(self::FORM_FIELD_PREFIX, '', $fieldName);
      $valuesWithProperFieldNames[$fieldId] = $submittedValue;
    }

    return $valuesWithProperFieldNames;
  }

  /**
   * Prepare data for submission.
   *
   * @param array $submittedValues
   *   Submitted form values.
   *
   * @return array
   *   Data prepared for API submission.
   *
   * @throws \Drupal\g2_reviews\Exception\InsufficientUserDataException
   *   Throw if user not logged in or doesn't have first or last name.
   */
  private function prepareFormValuesForSubmit(array $submittedValues): array {
    $productId = $submittedValues['product_id'];
    $userData = $this->g2CommonService->getUserData();
    $preparedData = [
      'product_id' => $productId,
      'user' => $userData,
      'terms_accepted' => $submittedValues['terms_and_conditions'] ? 'true' : 'false',
    ];

    $answers = [];
    $questionToAnswerTypes = $this->getQuestionToAnswerType($productId, $userData['email']);
    foreach ($submittedValues as $questionId => $submittedValue) {
      if (!isset($questionToAnswerTypes[$questionId])) {
        continue;
      }

      if (is_array($submittedValue)) {
        $submittedValue = array_values(array_filter($submittedValue));
      }
      $answerType = $questionToAnswerTypes[$questionId];
      if ($answerType === 'boolean') {
        $submittedValue = $submittedValue ? 'true' : 'false';
      }

      $answers[] = [
        'attributes' => [
          'question_id' => (string) $questionId,
          $answerType => $submittedValue,
        ],
      ];
    }

    $preparedData['relationships']['answers'] = $answers;

    return $preparedData;
  }

  /**
   * Get request to answer mapping.
   *
   * @param string $productId
   *   Product Id.
   * @param string $userEmail
   *   User Id.
   *
   * @return array
   *   Mapping questions to answers.
   */
  private function getQuestionToAnswerType(string $productId, string $userEmail): array {
    $g2FormFields = $this->getG2FormData($productId, $userEmail);

    $questionIdToAnswerType = [];
    foreach ($g2FormFields['data']['attributes']['relationships']['questions']['data'] as $g2FormField) {
      $answerType = array_keys($g2FormField['answer']);
      $questionIdToAnswerType[$g2FormField['id']] = reset($answerType);
    }

    return $questionIdToAnswerType;
  }

}
