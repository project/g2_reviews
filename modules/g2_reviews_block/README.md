# The G2 Reviews Block module

## Introduction

The G2 Reviews Block module is a block implementation for the displaying the
list of G2 product reviews.

## FAQ

Q: I enabled the module. How do I place a product reviews block?

A:
In order to place a block you need to perform the following steps:
* Go under `Administration » Structure » Block layout`
* Click on `Place a block` under a corresponding region and find the Category `G2`
* Place a `G2 Product Reviews List block` block and configure it by setting the
product ID value (make sure the ID is existent otherwise it won't draw the filters configuration form)
* Once you set a valid product ID you can generate the filters form which can be
exposed by checking several configurations inside the block config form.
* Review configurations, click save and check it on frontend part of the site.

Q: How do I place a review submit form?
* Same as for the previous question with only 1 difference. The block we need to
place is `G2 Product Review Iframe Form`.

Q: How do I get the filters form?

A: Each G2 product has it's own filters form with configured options and configurations.
In other words, each product will have it's own exposed filters form.
