/**
 * @file
 * G2 reviews block js processing.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Product reviews block admin processing.
   */
  Drupal.behaviors.g2ReviewsBlockAdmin = {
    attach: function (context, settings) {
      // Admin config form auto generate filter form.
      if ($('input[name="settings[product_id]"]').val()) {
        $('#edit-settings-generate-filters-form').click();
      }
    }
  };

})(jQuery, Drupal, drupalSettings);

