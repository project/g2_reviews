<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews_block\Plugin\Block;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\g2_reviews\Services\G2;
use Drupal\Core\Form\FormStateInterface;
use Drupal\g2_reviews\Services\G2Common;

/**
 * Provides a list with product reviews Block.
 *
 * @Block(
 *   id = "g2_product_reviews_list_block",
 *   admin_label = @Translation("G2 Product Reviews - List"),
 *   category = @Translation("G2"),
 * )
 */
class ProductReviewsListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The G2 API service.
   *
   * @var \Drupal\g2_reviews\Services\G2
   */
  protected G2 $g2;

  /**
   * The G2 Common service.
   *
   * @var \Drupal\g2_reviews\Services\G2Common
   */
  protected G2Common $g2Common;

  /**
   * The filters form configurator wrapper selector.
   *
   * @var string
   */
  protected string $filtersWrapper = 'filters-wrapper';

  /**
   * The filters date format.
   *
   * @var string
   */
  protected string $dateFormat = 'Y-m-d';

  /**
   * The constructor for ProductsListBlock block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\g2_reviews\Services\G2 $g2
   *   The G2 API service.
   * @param \Drupal\g2_reviews\Services\G2Common $g2common
   *   The G2 Common service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    G2 $g2,
    G2Common $g2common
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->g2 = $g2;
    $this->g2Common = $g2common;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('g2_reviews.api'),
      $container->get('g2_reviews.common')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $form = parent::blockForm($form, $form_state);

    $form['product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#description' => $this->t('The G2 product ID to work with'),
      '#default_value' => !empty($config['product_id']) ? $config['product_id'] : [],
      '#required' => TRUE,
    ];

    $form['review_form_renderer'] = [
      '#type' => 'radios',
      '#title' => $this->t('Review form renderer'),
      '#default_value' => 'iframe',
      '#options' => [
        'iframe' => $this->t('Iframe'),
        'form' => $this->t('Drupal form'),
      ],
    ];

    $form['generate_filters_form'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate form'),
      '#ajax' => [
        'wrapper' => $this->filtersWrapper,
        'event' => 'click',
        'callback' => [$this, 'getForm'],
        'method' => 'replace',
      ],
    ];

    $form['filters'] = [
      '#type' => 'fieldset',
      '#prefix' => "<div class='hidden' id='$this->filtersWrapper'>",
      '#suffix' => '</div>',
    ];
    $form['#attached']['library'][] = 'g2_reviews_block/g2_reviews_block';

    return $form;
  }

  /**
   * Ajax submit callback for generation G2 filters form.
   *
   * @param array $form
   *   The form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state data.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return the G2 filters form as ajax response.
   */
  public function getForm(array &$form, FormStateInterface $form_state): AjaxResponse {
    $values = $form_state->getValues();
    $config = $this->getConfiguration();
    $product_id = $values['settings']['product_id'];
    $response = new AjaxResponse();

    $form['filters'] = [
      '#type' => 'fieldset',
      '#prefix' => "<div id='$this->filtersWrapper'>",
      '#suffix' => '</div>',
    ];

    $product = $this
      ->g2
      ->getProducts(['filter' => ['product_id' => [$product_id]]]);

    if (!$product['data']) {
      $form['filters']['error'] = [
        '#markup' => $this->t('Could not find this product'),
      ];
      $response->addCommand(new ReplaceCommand("#$this->filtersWrapper", $form['filters']));
      return $response;
    }

    // Initial request for product reviews, so, we can take aggregates data.
    $reviews = $this->g2->getProductReviews($product_id);
    if (empty($reviews['meta'])) {
      $form['filters']['error'] = [
        '#markup' => $this->t('Could not load data for this product ID'),
      ];
      $response->addCommand(new ReplaceCommand("#$this->filtersWrapper", $form['filters']));
      return $response;
    }

    $form['filters']['#title'] = $this->t('Default filter configurator');

    $form['filters']['expose_filters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expose filters'),
      '#value' => !empty($config['filters']['expose_filters']),
      '#checked' => !empty($config['filters']['expose_filters']),
      '#description' => $this->t('A flag in order to expose filters form within the block or not.'),
      '#name' => 'filters[expose_filters]',
    ];

    $form['filters']['expose_pager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expose pager'),
      '#value' => !empty($config['filters']['expose_pager']),
      '#checked' => !empty($config['filters']['expose_pager']),
      '#description' => $this->t('A flag in order to expose pager within the block or not.'),
      '#name' => 'filters[expose_pager]',
    ];

    // Build filters form.
    $config_filters = !empty($config['filters']) ? $config['filters'] : [];
    $form['filters'] += $this
      ->g2Common
      ->buildProductFiltersform($product_id, $config_filters);

    $form['filters']['updated_at_gt'] = [
      '#type' => 'date',
      '#title' => $this->t('Reviews updated since date'),
      '#value' => !empty($config['filters']['updated_at_gt']) ? $config['filters']['updated_at_gt'] : '',
      '#description' => $this->t('Filters to all reviews updated since the date'),
      '#error_no_message' => TRUE,
      '#date_date_format' => $this->dateFormat,
      '#name' => 'filters[updated_at_gt]',
    ];

    $form['filters']['updated_at_lt'] = [
      '#type' => 'date',
      '#title' => $this->t('Reviews updated before the date'),
      '#value' => !empty($config['filters']['updated_at_lt']) ? $config['filters']['updated_at_lt'] : '',
      '#description' => $this->t('Filters to all reviews updated before the date'),
      '#error_no_message' => TRUE,
      '#date_date_format' => $this->dateFormat,
      '#name' => 'filters[updated_at_lt]',
    ];

    $max_options = [];
    foreach (range(1, 100) as $amount) {
      $max_options[$amount] = $amount;
    }
    $form['filters']['size'] = [
      '#type' => 'select',
      '#options' => $max_options,
      '#value' => !empty($config['filters']['size']) ? $config['filters']['size'] : 10,
      '#title' => $this->t('Items per page'),
      '#description' => $this->t('The items per page to show.'),
      '#name' => 'filters[size]',
    ];
    $form['filters']['page'] = [
      '#type' => 'number',
      '#value' => !empty($config['filters']['page']) ? $config['filters']['page'] : 1,
      '#title' => $this->t('Default page'),
      '#description' => $this->t('Set the default page offset.'),
      '#name' => 'filters[page]',
    ];
    $form_state->setRebuild(TRUE);

    $response->addCommand(new ReplaceCommand("#$this->filtersWrapper", $form['filters']));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getUserInput();
    $this->configuration['product_id'] = $values['settings']['product_id'];
    $this->configuration['filters'] = $values['filters'];
    $this->configuration['filters']['review_form_renderer'] = $values['settings']['review_form_renderer'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->getConfiguration();
    return $this->g2Common->buildProductReviewsWidget($config);
  }

}
