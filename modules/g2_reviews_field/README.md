# The G2 Reviews Field module

## Introduction

The G2 Reviews Field module is a field formatter and widget implementations
for the displaying the list of G2 product reviews as a field.

## FAQ

Q: I enabled the module. How do I place a product reviews field?

A:
In order to place a field you need to perform the following steps:
* Go under `Administration » Structure » Content types`
* Find any content type you need to integrate with the G2, so go to the manage fields section
* Add a new field and select the category `G2` and the name `G2: Reviews widget`
* Set a name and save it. Then go under `Manage display` tab section
* Find the created field and configure options form like filters or pager.
Items per page, date interval or default page number.
* Now create a node of the content type you selected and set a valid product ID.
Click save and go to the view of the node entity.

Same can be done with any entity, not only a node.
