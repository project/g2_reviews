<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\g2_reviews\Services\G2Common;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'G2: Reviews Widget' formatter.
 *
 * @FieldFormatter(
 *   id = "g2_reviews_widget_formatter",
 *   label = @Translation("G2: Reviews Widget"),
 *   field_types = {
 *     "g2_reviews_widget"
 *   }
 * )
 */
class G2ReviewsWidgetFormatter extends FormatterBase {

  /**
   * The G2 Common service.
   *
   * @var \Drupal\g2_reviews\Services\G2Common
   */
  protected G2Common $g2Common;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The filters date format.
   *
   * @var string
   */
  protected string $dateFormat = 'Y-m-d';

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\g2_reviews\Services\G2Common $g2common
   *   The G2 Common service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    string $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    string $label,
    string $view_mode,
    array $third_party_settings,
    G2Common $g2common,
    RendererInterface $renderer
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->g2Common = $g2common;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('g2_reviews.common'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Displays a rendered G2 reviews list widget for a particular product.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['expose_filters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expose filters'),
      '#default_value' => $this->getSetting('expose_filters'),
      '#description' => $this->t('A flag in order to expose filters form within the block or not.'),
    ];

    $form['expose_pager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expose pager'),
      '#default_value' => $this->getSetting('expose_pager'),
      '#description' => $this->t('A flag in order to expose pager within the block or not.'),
    ];

    $form['updated_at_gt'] = [
      '#type' => 'date',
      '#title' => $this->t('Reviews updated since date'),
      '#default_value' => $this->getSetting('updated_at_gt'),
      '#description' => $this->t('Filters to all reviews updated since the date'),
      '#error_no_message' => TRUE,
      '#date_date_format' => $this->dateFormat,
    ];

    $form['updated_at_lt'] = [
      '#type' => 'date',
      '#title' => $this->t('Reviews updated before the date'),
      '#default_value' => $this->getSetting('updated_at_lt'),
      '#description' => $this->t('Filters to all reviews updated before the date'),
      '#error_no_message' => TRUE,
      '#date_date_format' => $this->dateFormat,
    ];

    $max_options = [];
    foreach (range(1, 100) as $amount) {
      $max_options[$amount] = $amount;
    }
    $form['size'] = [
      '#type' => 'select',
      '#options' => $max_options,
      '#default_value' => $this->getSetting('size'),
      '#title' => $this->t('Items per page'),
      '#description' => $this->t('The items per page to show.'),
    ];
    $form['page'] = [
      '#type' => 'number',
      '#default_value' => $this->getSetting('page'),
      '#title' => $this->t('Default page'),
      '#description' => $this->t('Set the default page offset.'),
    ];

    $form['review_form_renderer'] = [
      '#type' => 'radios',
      '#title' => $this->t('Review form renderer'),
      '#default_value' => 'iframe',
      '#options' => [
        'iframe' => $this->t('Iframe'),
        'form' => $this->t('Drupal form'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'size' => 10,
      'page' => 1,
      'expose_filters' => FALSE,
      'expose_pager' => FALSE,
      'updated_at_gt' => '',
      'updated_at_lt' => '',
      'review_form_renderer' => 'iframe',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   *   Render Exception.
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $config = [
      'product_id' => $items->first()->value,
      'filters' => $this->getSettings(),
    ];
    // Build the widget instance with the corresponding configs.
    $widget = $this->g2Common->buildProductReviewsWidget($config);

    return [
      [
        '#markup' => $this->renderer->render($widget),
      ],
    ];
  }

}
