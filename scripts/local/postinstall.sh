#!/bin/bash

#
#Set Colors
#
bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

purple=$(tput setaf 171)
red=$(tput setaf 1)
green=$(tput setaf 76)
tan=$(tput setaf 3)
blue=$(tput setaf 38)

#
# Headers and  Logging
#
e_header() {
    printf "\n${bold}${purple}==========  %s  ==========${reset}\n" "$@"
}
e_arrow() {
    printf "➜ $@\n"
}
e_success() {
    printf "${green}✔ %s${reset}\n" "$@"
}
e_error() {
    printf "${red}✖ %s${reset}\n" "$@"
}
e_warning() {
    printf "${tan}➜ %s${reset}\n" "$@"
}
e_underline() {
    printf "${underline}${bold}%s${reset}\n" "$@"
}
e_bold() {
    printf "${bold}%s${reset}\n" "$@"
}
e_note() {
    printf "${underline}${bold}${blue}Note:${reset}  ${blue}%s${reset}\n" "$@"
}

type_exists() {
    if [[ $(type -P $1) ]]; then
      return 0
    fi
    return 1
}

is_os() {
    if [[ "${OSTYPE}" == $1* ]]; then
      return 0
    fi
    return 1
}

preparation() {
    e_header "Preparation"

    # Install gnu-sed for MacOs.
#     if is_os "darwin"; then
#         e_note "[$(date +%FT%TZ)] You are on a mac"
#         e_arrow "Check brew"
#
#         if type_exists 'brew'; then
#             e_note "[$(date +%FT%TZ)] Brew found"
#             e_arrow "Install gnu-sed"
#             brew install gnu-sed
#             e_arrow "Change alias"
#             export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
#         else
#             e_error "[Error] Brew is not installed."
#             exit 1
#         fi
#     fi

    # Verify if "docker" is installed.
    if ! type_exists 'docker'; then
        e_error "[Error] Docker is not installed."
    fi

    # Verify if "docker-compose" is installed.
    if ! type_exists 'docker-compose'; then
        e_error "[Error] Docker-compose is not installed."
    fi

    # Verify if "ddev" exists and version is up-to-date.
    if ! type_exists 'ddev'; then
        e_note "[$(date +%FT%TZ)] Install ddev"

        e_arrow "Install ddev"
        sudo curl -L https://raw.githubusercontent.com/drud/ddev/master/scripts/install_ddev.sh | bash
    else
         e_note "[$(date +%FT%TZ)] ddev already installed"
    fi

    echo ""
    e_warning "[$(date +%FT%TZ)] BEGIN task: Check nvm."
    if which nvm > /dev/null
    then
        nvm install 12
        nvm use 12
    else
        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | dash
        export NVM_DIR="$HOME/.nvm"
        [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
        nvm current
        nvm install 12
        nvm use 12
    fi

    echo ""
    e_warning "[$(date +%FT%TZ)] BEGIN task: Check yarn."
    if which yarn > /dev/null
    then
        e_note "[$(date +%FT%TZ)] yarn already installed"
    else
       npm install --global yarn
       e_warning "[$(date +%FT%TZ)] yarn installed"
    fi

#    echo ""
#    e_warning "[$(date +%FT%TZ)] Install Composer prestissimo"
#    composer global require hirak/prestissimo
#    e_warning "[$(date +%FT%TZ)] END task: Install Composer prestissimo"

    export DOCKER_CLIENT_TIMEOUT=120
    export COMPOSE_HTTP_TIMEOUT=120
}

install_script() {
    preparation
}

install_script
