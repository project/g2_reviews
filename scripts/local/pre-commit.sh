#!/bin/sh

# Set Colors
bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

purple=$(tput setaf 171)
red=$(tput setaf 1)
green=$(tput setaf 76)
tan=$(tput setaf 3)
blue=$(tput setaf 38)

# Headers and  Logging
e_header() {
    printf "\n${bold}${purple}==========  %s  ==========${reset}\n" "$@"
}
e_arrow() {
    printf "➜ $@\n"
}
e_success() {
    printf "${green}✔ %s${reset}\n" "$@"
}
e_error() {
    printf "${red}✖ %s${reset}\n" "$@"
}
e_warning() {
    printf "${tan}➜ %s${reset}\n" "$@"
}
e_underline() {
    printf "${underline}${bold}%s${reset}\n" "$@"
}
e_bold() {
    printf "${bold}%s${reset}\n" "$@"
}
e_note() {
    printf "${underline}${bold}${blue}Note:${reset}  ${blue}%s${reset}\n" "$@"
}


PROJECT=`php -r "echo dirname(dirname(dirname(realpath('$0'))));"`
# TODO: Right now validation will be proceed only for modules/unity folder. Do we want to add any other dir?
STAGED_FILES_CMD=`git diff --cached --name-only --diff-filter=ACMR HEAD | grep "\\.install\|\\.module\|\\.inc\|\\.php\|\\.yml"`
#STAGED_FILES_CMD=`git diff --name-only --diff-filter=ACMR HEAD`

# Determine if a file list is passed
if [ "$#" -eq 1 ]
then
  oIFS=$IFS
  IFS='
  '
  SFILES="$1"
  IFS=$oIFS
fi
SFILES=${SFILES:-$STAGED_FILES_CMD}

for FILE in $SFILES
do
  docker run --rm -v $(pwd):/data cytopia/phplint $FILE
  if [ $? != 0 ]
    then
      e_error "Fix the error before commit."
      exit 1
  fi
  FILES="$FILES $FILE"
done

if [ "$FILES" != "" ]
then
  # Drupal standards.
  e_header "Running Drupal Standards..."
  docker run --rm -v $(pwd):/app mparker17/phpcs-drupal $FILES
  if [ $? != 0 ]
    then
      e_warning "Fixing..."
      docker run --rm -v $PWD:/app mparker17/phpcbf-drupal $FILES
    else
      e_success "Done"
  fi

  # Drupal BestPractices.
  e_header "Running Drupal BestPractices..."
  docker run --rm -v $(pwd):/app mparker17/phpcs-drupalpractice $FILES
  if [ $? != 0 ]
    then
      e_warning "Fixing..."
      docker run --rm -v $PWD:/app mparker17/phpcbf-drupalpractice $FILES
    else
      e_success "Done"
  fi

#  e_header "Running PHPStan..."
#  docker run --rm -v $(pwd):/app mparker17/phpstan-drupal $FILES
#  if [ $? != 0 ]
#    then
#      e_error "Fix the error(s) before commit."
#      exit 1
#    else
#      e_success "Done"
#  fi

fi

exit $?
