#!/bin/bash

# Check os type.
os_type() {
    if [[ "${OSTYPE}" == $1* ]]; then
      return 0
    fi
    return 1
}

# is Linux.
if os_type "linux-gnu"; then
    scripts/security_checkers/local-php-security-checker-linux
fi

# is MacOs.
if os_type "darwin"; then
    scripts/security_checkers/local-php-security-checker-darwin
fi

# is Windows.
if os_type "msys" || os_type "cygwin"; then
    scripts/security_checkers/local-php-security-checker-win.exe
fi
