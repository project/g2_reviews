<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\g2_reviews\Services\G2Common;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The G2ReviewsWidgetAjaxController controller class.
 */
class G2ReviewsWidgetAjaxController extends ControllerBase {

  /**
   * The G2 Common service.
   *
   * @var \Drupal\g2_reviews\Services\G2Common
   */
  protected G2Common $g2Common;

  /**
   * The Plugin Block Manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected BlockManagerInterface $blockManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The request stack used to determine current time.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new G2ReviewsWidgetAjaxController object.
   *
   * @param \Drupal\g2_reviews\Services\G2Common $g2common
   *   The G2 Common service.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The Plugin Block Manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to determine the current time.
   */
  public function __construct(
    G2Common $g2common,
    BlockManagerInterface $block_manager,
    RendererInterface $renderer,
    RequestStack $request_stack
  ) {
    $this->g2Common = $g2common;
    $this->blockManager = $block_manager;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('g2_reviews.common'),
      $container->get('plugin.manager.block'),
      $container->get('renderer'),
      $container->get('request_stack')
    );
  }

  /**
   * Ajax callback for reviews block in context of filters and paginator.
   *
   * @param string $product_id
   *   The G2 product UUID string.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an ajax response.
   *
   * @throws \Exception
   *   Exception.
   */
  public function ajax(string $product_id): AjaxResponse {
    $response = new AjaxResponse();
    // First get all request params.
    $params = $this->requestStack->getMasterRequest()->request->all();

    $wrapper = "g2-product-$product_id";
    $config = $params['widget_config'];

    if (empty($config['filters'])) {
      throw new MissingDataException('Missing "filters" request data');
    }

    // Set the correct pagination.
    if (!empty($params['page'])) {
      $config['filters']['page'] = $params['page'];
    }

    // Set filtration.
    if (!empty($params['filter'])) {
      // Reset the pager.
      $config['filters']['page'] = 1;

      $filter_name = $params['filter']['name'];
      $filter_value = $params['filter']['value'];
      $filter_checked = $params['filter']['checked'];

      // Add a new filter option.
      if ($filter_checked === 'true') {
        $config['filters'][$filter_name][] = $filter_value;
      }
      else {
        // Unset the selected filter option from the filter.
        foreach ($config['filters'][$filter_name] as $key => $value) {
          if ((int) $value == (int) $filter_value) {
            unset($config['filters'][$filter_name][$key]);
          }
        }
        $config['filters'][$filter_name] = array_values($config['filters'][$filter_name]);
      }
    }

    // Build the widget instance with the corresponding configs.
    $widget = $this->g2Common->buildProductReviewsWidget($config);
    // Render the widget.
    $render = $this->renderer->render($widget);
    // Return the rendered widget to the wrapper element.
    $response->addCommand(new ReplaceCommand("#$wrapper", $render));

    return $response;
  }

}
