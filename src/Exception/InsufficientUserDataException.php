<?php

namespace Drupal\g2_reviews\Exception;

use Exception;

/**
 * Class InsufficientUserDataException.
 *
 * @package Drupal\g2_review_form\Exception
 */
class InsufficientUserDataException extends Exception {}
