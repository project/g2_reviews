<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configure G2ConfigForm form.
 */
class G2ConfigForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Config settings.
   *
   * @var string
   */
  public const SETTINGS = 'g2_reviews.config';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'g2_reviews_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#default_value' => $config->get('token') ?: '',
      '#description' => $this->t('The secret token.'),
      '#required' => TRUE,
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $config->get('domain') ?: '',
      '#description' => $this->t('The API domain.'),
      '#required' => TRUE,
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#default_value' => (bool) $config->get('debug'),
      '#description' => $this->t('Debug the API responses or not.'),
      '#required' => FALSE,
    ];

    $form['partner_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner Name'),
      '#default_value' => $config->get('partner_name') ?: '',
      '#description' => $this->t('This URL is specific to the vendor using this functionality, and is used to access the G2 API endpoint (for example, https://www.g2.com/partnerships/[PartnerName]/).'),
    ];

    $form['user_data_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User fields mapping.'),
    ];

    $form['user_data_fieldset']['first_name_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First name field'),
      '#required' => TRUE,
      '#default_value' => $config->get('first_name_field') ?: '',
      '#description' => $this->t('Use Drupal Token to specify which field is responsible for User first name. E.g. [current-user:field_first_name]'),
    ];

    $form['user_data_fieldset']['last_name_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name field'),
      '#required' => TRUE,
      '#default_value' => $config->get('last_name_field') ?: '',
      '#description' => $this->t('Use Drupal Token to specify which field is responsible for User last name. E.g. [current-user:field_last_name]'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('token', $form_state->getValue('token'))
      ->set('domain', $form_state->getValue('domain'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('partner_name', $form_state->getValue('partner_name'))
      ->set('first_name_field', $form_state->getValue('first_name_field'))
      ->set('last_name_field', $form_state->getValue('last_name_field'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
