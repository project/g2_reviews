<?php

namespace Drupal\g2_reviews\Interfaces;

/**
 * Interface G2ReviewInterface.
 *
 * @package Drupal\g2_reviews\Interfaces
 */
interface G2ReviewInterface {

  /**
   * Generate Iframe for G2 Review.
   *
   * @param string $productId
   *   The product id.
   * @param string $width
   *   The iframe width.
   * @param string $height
   *   The iframe height.
   *
   * @return array
   *   Result Response.
   *
   * @throws \Exception
   */
  public function iframeData(string $productId, string $width = '100%', string $height = '100%'): array;

}
