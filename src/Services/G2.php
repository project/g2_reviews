<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews\Services;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\RequestOptions;

/**
 * Class G2.
 *
 * @package Drupal\g2_reviews\Services
 */
class G2 {

  /*
   * Add string translation trait.
   */
  use StringTranslationTrait;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Returns the default http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * The G2 base API url.
   *
   * @var string
   */
  protected $domain;

  /**
   * The G2 authorization API secret token.
   *
   * @var string
   */
  protected $token;

  /**
   * The G2 flag that allows debugging API responses or not.
   *
   * @var string
   */
  protected $debug;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * Gets the current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new G2Api object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The guzzle http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current account.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    Client $http_client,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    AccountProxyInterface $current_user,
    CacheBackendInterface $cache,
    TimeInterface $time
  ) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->currentUser = $current_user;
    $this->cache = $cache;
    $this->time = $time;

    // Get admin API configs.
    $config = $this->configFactory->get('g2_reviews.config');

    if (empty($config->get('token')) || empty($config->get('domain'))) {
      $this
        ->loggerFactory
        ->get('g2')
        ->error($this->t(
          "G2: please, go here: @here and add all necessary configs or you won't be able to use the system.",
          ['@here' => Url::fromRoute('g2.system')->toString()]
        ));
    }
    $this->token = $config->get('token');
    $this->domain = $config->get('domain');
    $this->debug = $config->get('debug');
  }

  /**
   * Set the options for the request processing.
   *
   * @param string $productId
   *   The product UUID string.
   *
   * @return array
   *   Return the options array config for the request instance.
   */
  public function setOptions(string $productId = NULL): array {
    $options = [
      RequestOptions::HEADERS => [
        'Authorization' => 'Token token=' . $this->token,
        'Content-Type' => 'application/vnd.api+json',
      ],
    ];

    // Add the product UUID (if exists) filter to the options data.
    if (!empty($productId)) {
      $options[RequestOptions::JSON] = [
        'filter' => [
          'product_id' => $productId,
        ],
      ];
    }

    return $options;
  }

  /**
   * Get the list of products.
   *
   * @param array $data
   *   The API resource data for filtering.
   *
   * @return array
   *   Returns the list of products from G2 platform.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  public function getProducts(array $data = []): array {
    $endpoint = '/api/2018-01-01/syndication/products';
    $url = $this->domain . '' . $endpoint;
    $options = $this->setOptions();
    // Add filtering in case it was set.
    if (!empty($data)) {
      $options[RequestOptions::JSON] = $data;
    }
    return $this->processResponse($url, $options);
  }

  /**
   * Get the list of a product reviews.
   *
   * @param string $productId
   *   The product ID.
   * @param array $data
   *   The API resource data for filtering.
   *
   * @return array
   *   Returns the list of syndicate reviews from G2.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  public function getProductReviews(string $productId, array $data = []): array {
    $endpoint = '/api/2018-01-01/syndication/reviews';
    $url = $this->domain . '' . $endpoint;
    $options = $this->setOptions();
    // Add filtering in case it was set.
    if (!empty($data)) {
      $options[RequestOptions::JSON] = $data;
    }
    $options[RequestOptions::JSON]['filter']['product_id'] = $productId;

    return $this->processResponse($url, $options);
  }

  /**
   * Get the list of a product snippets.
   *
   * @param string $productId
   *   The product ID.
   * @param array $data
   *   The API resource data for filtering.
   *
   * @return array
   *   Returns the list of syndicate snippets from G2.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  public function getProductSnippets(string $productId, array $data = []): array {
    $endpoint = '/api/2018-01-01/syndication/snippets';
    $url = $this->domain . '' . $endpoint;
    $options = $this->setOptions($productId);

    return $this->processResponse($url, $options);
  }

  /**
   * Get the list of comments from the product reviews.
   *
   * @param string $productId
   *   The product ID.
   * @param array $data
   *   The API resource data for filtering.
   *
   * @return array
   *   Returns the list of syndicate reviews comments from G2.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  public function getProductReviewsComments(string $productId, array $data = []): array {
    $endpoint = '/api/2018-01-01/syndication/reviews/comments';
    $url = $this->domain . '' . $endpoint;
    $options = $this->setOptions($productId);

    return $this->processResponse($url, $options);
  }

  /**
   * Get the embedded product review form.
   *
   * @param string $productId
   *   The product ID.
   * @param string $email
   *   The email of the person who is writing the review.
   *
   * @return array
   *   Returns the review form G2.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  public function getProductEmbeddedtReviewForm(string $productId, string $email): array {
    $endpoint = '/api/2018-01-01/syndication/embedded_reviews/review_form';
    $url = $this->domain . '' . $endpoint;
    $options = $this->setOptions();
    $options[RequestOptions::JSON] = [
      'product_id' => $productId,
      'user' => ['email' => $email],
    ];
    return $this->processResponse($url, $options, 'POST');
  }

  /**
   * Submit a review for a product.
   *
   * @param array $data
   *   The product review form data.
   *
   * @return array
   *   Returns the product form G2.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  public function submitProductReview(array $data): array {
    $endpoint = '/api/2018-01-01/syndication/embedded_reviews';
    $url = $this->domain . '' . $endpoint;
    $options = $this->setOptions();
    $data = [
      'format' => RequestOptions::JSON,
      'review_form' => [
        'attributes' => [
          'product_id' => $data['product_id'],
          'user' => $data['user'],
          'terms_accepted' => $data['terms_accepted'],
          'relationships' => [
            'answers' => (object) $data['relationships']['answers'],
          ],
        ],
      ],
    ];
    $prepared_data = json_encode($data);
    $options[RequestOptions::BODY] = $prepared_data;

    return $this->processResponse($url, $options, 'POST');
  }

  /**
   * Processing the response from G2.
   *
   * @param string $url
   *   The API resource url.
   * @param array $config
   *   The API config like access token.
   * @param string $method
   *   The request method GET/POST etc.
   *
   * @return array
   *   The resource response from G2.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Guzzle Exception.
   */
  protected function processResponse(string $url, array $config, string $method = 'GET'): array {
    $cid = "{$url}:{$method}:" . Json::encode($config);
    $cache = $this->cache->get($cid);
    if ($cache) {
      return Json::decode($cache->data, TRUE);
    }

    try {
      $response = $this->httpClient->request($method, $url, $config);
      $code = $response->getStatusCode();
      // Successful response.
      if ($code === 200) {
        // Set cache for 24h.
        $this->cache->set($cid, (string) $response->getBody(), $this->time->getRequestTime() + 86400);
        // Return the response converted to array.
        return Json::decode((string) $response->getBody(), TRUE);
      }
    }
    catch (\Exception $e) {
      if (!empty($this->debug)) {
        // Write a log into the db once there are any issues.
        $this
          ->loggerFactory
          ->get('g2')
          ->error($this->t(
            "G2: An error occured: @error.",
            ['@error' => $e->getMessage()]
          ));
      }
      return ['error' => $e->getMessage()];
    }
  }

}
