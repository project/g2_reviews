<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\g2_reviews\Exception\InsufficientUserDataException;
use Drupal\g2_reviews\Form\G2ConfigForm;

/**
 * The G2 Common service class.
 */
class G2Common {

  /* Add string translation trait. */
  use StringTranslationTrait;

  /**
   * The G2 API service.
   *
   * @var \Drupal\g2_reviews\Services\G2
   */
  protected G2 $g2;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The filters date format.
   *
   * @var string
   */
  protected string $dateFormat = 'Y-m-d';

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private AccountInterface $account;

  /**
   * Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  private Token $tokenService;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new G2Common object.
   *
   * @param \Drupal\g2_reviews\Services\G2 $g2
   *   The G2 API service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account interface.
   * @param \Drupal\Core\Utility\Token $tokenService
   *   Token service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(
    G2 $g2,
    DateFormatterInterface $date_formatter,
    AccountInterface $account,
    Token $tokenService,
    ConfigFactoryInterface $configFactory
  ) {
    $this->g2 = $g2;
    $this->dateFormatter = $date_formatter;
    $this->account = $account;
    $this->tokenService = $tokenService;
    $this->configFactory = $configFactory;
  }

  /**
   * Build a product's filters form.
   *
   * @param string $productId
   *   The product UUID string.
   * @param array $defaults
   *   The default values for filters.
   *
   * @return array
   *   Return the build form array with product filters.
   */
  public function buildProductFiltersForm(string $productId, array $defaults = []): array {
    $form = [];
    // Initial request for product reviews, so, we can take aggregates data.
    $reviews = $this->g2->getProductReviews($productId);
    if (empty($reviews['meta'])) {
      return $form;
    }

    // Loop through each filter in part, so, build it as a form element.
    foreach ($reviews['meta']['aggregates'] as $filter) {
      $filter_name = $filter['filter_name'];
      $filter_label = str_replace('_', ' ', $filter_name);

      // Prepare the filter wrapper.
      $form[$filter_name] = [
        '#type' => 'fieldset',
        '#title' => $filter_label,
      ];

      foreach ($filter['collection'] as $option) {
        $filter_option_id = $option['id'];
        $title = $this->t(
          '@option_label, (@count)',
          ['@option_label' => $option['text'], '@count' => $option['count']]
        );

        $checked = FALSE;
        if (!empty($defaults[$filter_name]) && in_array($filter_option_id, $defaults[$filter_name])) {
          $checked = TRUE;
        }

        $form[$filter_name][$filter_option_id] = [
          '#type' => 'checkbox',
          '#tree' => TRUE,
          '#title' => $title,
          '#attributes' => [
            'value' => $filter_option_id,
            'data-filter_name' => $filter_name,
          ],
          '#name' => "filters[$filter_name][]",
          '#checked' => $checked,
          '#value' => $checked,
        ];
      }
    }
    return $form;
  }

  /**
   * Build the product reviews widget array form data, so, it can be rendered.
   *
   * @param array $config
   *   The widget config data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function buildProductReviewsWidget(array $config): array {
    $product_id = $config['product_id'];
    $widget_config = $config;

    if (empty($product_id)) {
      return [];
    }

    $reviewFormRoute = ($config["filters"]["review_form_renderer"] === 'iframe')
      ? 'g2_review_form.iframe'
      : 'g2_review_form.form';

    $requestFilters = $this->prepareRequestFilters($config);
    $data = $this->g2->getProductReviews($product_id, $requestFilters);
    $widget = [
      '#theme' => 'g2_product_reviews_widget_list',
      '#reviews' => $this->getReviews($data),
      '#product_id' => $product_id,
      '#review_form_route' => $reviewFormRoute,
    ];

    if (!empty($config['filters']['expose_filters'])) {
      $widget['#filters_form'] = $this->prepareFiltersForm($config);
    }

    if (!empty($config['filters']['expose_pager'])) {
      $widget['#pager'] = [
        '#theme' => 'g2_product_reviews_widget_pager',
        '#page' => (int) !empty($config['filters']['page']) ? $config['filters']['page'] : 1,
        '#page_count' => $data['meta']['page_count'],
      ];
    }

    $widget['#attached']['library'][] = 'g2_reviews/g2_star';
    $widget['#attached']['library'][] = 'g2_reviews/g2_reviews_widget';
    $widget['#attached']['library'][] = 'g2_reviews/g2_ajax_loader';
    // Attach current configuration into drupal settings.
    $widget['#attached']['drupalSettings']['g2reviewsWidgets'][$product_id] = $widget_config;

    return $widget;
  }

  /**
   * Prepare form for filters.
   *
   * @param array $config
   *   Widget config.
   *
   * @return array
   *   Filters.
   */
  private function prepareFiltersForm(array $config): array {
    $productId = $config['product_id'];
    // Build filters form and exposed it.
    $form = $this->buildProductFiltersForm($productId, $config['filters']);
    $filtersForm = [];
    foreach ($form as $filter_element) {
      $title = $filter_element['#title'];
      unset($filter_element['#type']);
      unset($filter_element['#title']);

      $filtersForm[] = [
        '#theme' => 'g2_filter_element',
        '#product_id' => $productId,
        '#title' => $title,
        '#filter' => $filter_element,
      ];
    }

    return $filtersForm;
  }

  /**
   * Prepare request filters.
   *
   * @param array $config
   *   Widget config.
   *
   * @return array
   *   Request filters.
   */
  private function prepareRequestFilters(array $config): array {
    $filters = [];

    // Format NPS Score.
    $nps_score = [];
    if (!empty($config['filters']['nps_score'])) {
      foreach ($config['filters']['nps_score'] as $score) {
        $nps_score[] = (int) $score;
      }
      $filters['filter']['nps_score'] = $nps_score;
    }

    if (!empty($config['filters']['size'])) {
      $filters['page']['size'] = $config['filters']['size'];
    }

    if (!empty($config['filters']['page'])) {
      $filters['page']['number'] = $config['filters']['page'];
    }

    $excludedItems = [
      'expose_filters',
      'expose_pager',
      'nps_score',
      'size',
      'page',
      'review_form_renderer',
    ];
    foreach ($config['filters'] as $filter_name => $filter) {
      if (empty($filter) || in_array($filter_name, $excludedItems)) {
        continue;
      }
      $filters['filter'][$filter_name] = array_values($filter);
    }

    return $filters;
  }

  /**
   * Get reviews.
   *
   * @param array $data
   *   Reviews data received from API.
   *
   * @return array
   *   Reviews.
   */
  private function getReviews(array $data): array {
    $reviews = [];
    foreach ($data['data'] as $review) {
      $id = $review['id'];
      $reviews[$id] = $review['attributes'];
      $reviews[$id]['id'] = $id;
      $reviews[$id]['type'] = $review['type'];
      $reviews[$id]['star_rating_text'] = $this->t('@rating out of @max', [
        '@rating' => $review['attributes']['star_rating'],
        '@max' => 5,
      ]);

      // Format date into G2 default.
      foreach (['submitted_at', 'published_at'] as $date_key) {
        $reviews[$id][$date_key] = $this
          ->dateFormatter
          ->format(strtotime($reviews[$id][$date_key]), 'custom', 'M d, Y');
      }
    }

    return $reviews;
  }

  /**
   * Method checks if user logged in and have all necessary data.
   *
   * @return array
   *   User email, first name and last name.
   *
   * @throws \Drupal\g2_reviews\Exception\InsufficientUserDataException
   *   Throw if user not logged in or doesn't have first or last name.
   */
  public function getUserData(): array {
    if ($this->account->isAnonymous()) {
      throw new InsufficientUserDataException('You should be logged in to submit a review.');
    }

    $firstNameToken = $this->configFactory->get(G2ConfigForm::SETTINGS)->get('first_name_field');
    if (!$firstNameToken) {
      throw new ConfigValueException('First name field is not configured.');
    }
    $firstName = $this->tokenService->replace($firstNameToken);
    if (!$firstName || $firstName === $firstNameToken) {
      throw new InsufficientUserDataException('First name should be set in profile to submit review form');
    }

    $lastNameToken = $this->configFactory->get(G2ConfigForm::SETTINGS)->get('last_name_field');
    if (!$lastNameToken) {
      throw new ConfigValueException('Last name field is not configured.');
    }
    $lastName = $this->tokenService->replace($lastNameToken);
    if (!$lastName || $lastName === $lastNameToken) {
      throw new InsufficientUserDataException('Last name should be set in profile to submit review form');
    }

    return [
      'email' => $this->account->getEmail(),
      'first_name' => $firstName,
      'last_name' => $lastName,
    ];
  }

}
