<?php

declare(strict_types = 1);

namespace Drupal\g2_reviews\Services;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\g2_reviews\Interfaces\G2ReviewInterface;
use GuzzleHttp\Client;

/**
 * Class G2ReviewIframe.
 *
 * @package Drupal\g2_reviews\Services
 */
class G2ReviewIframe implements G2ReviewInterface {

  /*
   * Add string translation trait.
   */
  use StringTranslationTrait;

  /**
   * Returns the default http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * The G2 base API url.
   *
   * @var string
   */
  protected $domain;

  /**
   * The G2 authorization API secret token.
   *
   * @var string
   */
  protected $token;

  /**
   * G2 Partner name.
   *
   * @var string
   */
  protected $partnerName;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected LoggerChannelFactory $loggerFactory;

  /**
   * Gets the current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The G2 Common service.
   *
   * @var \Drupal\g2_reviews\Services\G2Common
   */
  protected G2Common $g2Common;

  /**
   * Constructs a new G2Api object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The guzzle http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current account.
   * @param \Drupal\g2_reviews\Services\G2Common $g2common
   *   The G2 Common service.
   */
  public function __construct(
    Client $http_client,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactory $logger_factory,
    AccountProxyInterface $current_user,
    G2Common $g2common
  ) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->currentUser = $current_user;
    $this->g2Common = $g2common;

    // Get admin API configs.
    $config = $this->configFactory->get('g2_reviews.config');

    if (empty($config->get('token')) || empty($config->get('domain')) || empty($config->get('partner_name'))) {
      $this
        ->loggerFactory
        ->get('type')
        ->error($this->t(
          "G2 system: please, go here: @here and add all necessary configs or you won't be able to use the system.",
          ['@here' => Url::fromRoute('g2.system')->toString()]
        ));
    }
    $this->token = $config->get('token');
    $this->domain = $config->get('domain');
    $this->partnerName = $config->get('partner_name');
  }

  /**
   * {@inheritdoc}
   */
  public function iframeData(string $productId, string $width = '100%', string $height = '100%'): array {
    $state = $this->getState($productId);
    $userData = $this->g2Common->getUserData();
    return [
      'iframe' => "<iframe src='{$this->domain}/partnerships/{$this->partnerName}/users/login.embed?state={$state}&email={$userData['email']}&first_name={$userData['first_name']}&last_name={$userData['last_name']}' style='width:{$width}; height:{$height};'></iframe>",
      'domain' => $this->domain,
    ];
  }

  /**
   * Get iframe state password.
   *
   * @param string $productId
   *   Product id for state request.
   *
   * @return string
   *   Iframe state signature.
   *
   * @throws \Exception
   *   Error on signature request.
   */
  protected function getState(string $productId): string {
    $endpoint = '/partnerships/' . $this->partnerName . '/tokens';
    $options = [
      'form_params' => [
        'api_token' => $this->token,
        'product_id' => $productId,
      ],
    ];
    $response = $this->handleRequest("{$this->domain}{$endpoint}", $options, 'POST');
    if (!isset($response['state'])) {
      throw new \Exception($this->t('Something went wrong. Try again.'));
    }
    return $response['state'];
  }

  /**
   * Processing the response from G2.
   *
   * @param string $url
   *   The API resource url.
   * @param array $config
   *   The API config like access token.
   * @param string $method
   *   The request method GET/POST etc.
   *
   * @return array
   *   The resource response from G2.
   */
  protected function handleRequest(string $url, array $config, string $method = 'GET'): array {
    $resultResponse = [];
    try {
      $response = $this->httpClient->post($url, [
        'verify' => TRUE,
      ] + $config);
      $code = $response->getStatusCode();
      // Successful response.
      if ($code === 200) {
        // Return the response converted to array.
        $resultResponse = Json::decode($response->getBody()->getContents());
      }
    }
    catch (\Exception $e) {
      // Write a log into the db once there are any issues.
      $this
        ->loggerFactory
        ->get('type')
        ->error($this->t(
          "G2 system: An error occured: @error.",
          ['@error' => $e->getMessage()]
        ));

      $resultResponse = ['error' => $e->getMessage()];
    }
    return $resultResponse;
  }

}
