<?php

namespace Drupal\Tests\g2\Unit;

use Drupal\Core\Cache\CacheBackendInterface;
use GuzzleHttp\Client;
use Drupal\Tests\UnitTestCase;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\g2_reviews\Services\G2;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * The unit test class for the "G2 service".
 *
 * @coversDefaultClass \Drupal\g2_reviews\Services\G2
 * @group g2
 */
class G2ApiTest extends UnitTestCase {

  /**
   * The entity bundle info service.
   *
   * @var \Drupal\g2_reviews\Services\G2
   */
  protected $g2Api;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Prophesize those services which calls the database. We don't need it.
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $current_user = $this->prophesize(AccountProxyInterface::class);
    $cache = $this->prophesize(CacheBackendInterface::class);
    $datetime = $this->prophesize(TimeInterface::class);

    // Emulate services and call them for every unit test case.
    $container = new ContainerBuilder();
    $container->set('logger.factory', $logger_factory->reveal());
    $container->set('current_user', $current_user->reveal());
    $container->set('cache.default', $cache->reveal());
    $container->set('datetime.time', $datetime->reveal());
    \Drupal::setContainer($container);

    // Emulate the config values stack, so we won't touch the database.
    $config_factory = $this->getConfigFactoryStub([
      'g2_reviews.config' => [
        // Use sandbox connector params.
        'token' => '<token>',
        'domain' => '<domain>',
        // Turn off debug mode, so we won't touch database logger service.
        'debug' => FALSE,
      ],
    ]);

    $this->g2Api = new G2(
      // We don't emulate/mock the http client as it's required to perform a
      // real request to the API.
      new Client(),
      $config_factory,
      \Drupal::service('logger.factory'),
      \Drupal::service('current_user'),
      \Drupal::service('cache.default'),
      \Drupal::service('datetime.time')
    );
  }

  /**
   * The data provider.
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $product_id
   *   - $fail_message
   *   - $structure_mismatch
   */
  public function configDataProvider(): array {
    return [
      [
        'product_id' => '<product_id>',
        'fail_message' => 'Could not load any data from the API',
        'structure_mismatch' => 'Data structure mismatch',
      ],
    ];
  }

  /**
   * Checks if the service is initiated or not.
   */
  public function testG2Api() {
    $this->assertNotNull($this->g2Api, 'Class service was not initiated');
  }

  /**
   * Test getting the list of the latest products from G2.
   */
  public function testGetProducts() {
    // Try to get the list of products without any filter. By default it will
    // return 10 items.
    $products = $this->g2Api->getProducts();

    $this->assertNotNull($products, 'Could not load any data from the API');

    // Check the response structure.
    $this->assertEquals(
      ['data', 'links', 'meta'],
      array_keys($products),
      'Data structure mismatch'
    );
  }

  /**
   * Test getting the list of sindicate reviews from G2.
   *
   * @dataProvider configDataProvider
   */
  public function testGetProductReviews(string $product_id, string $fail_message, string $structure_mismatch) {
    // Try to get the list of product reviews without any filter. By default it
    // will return 10 items.
    $product_reviews = $this->g2Api->getProductReviews($product_id);

    $this->assertNotNull($product_reviews, $fail_message);

    // Check the response structure.
    $this->assertEquals(
      ['data', 'meta'],
      array_keys($product_reviews),
      $structure_mismatch
    );
  }

  /**
   * Test getting the list of sindicate snippets from G2.
   *
   * @dataProvider configDataProvider
   */
  public function testGetProductSnippets(string $product_id, string $fail_message, string $structure_mismatch) {
    // Try to get the list of product snippets without any filter. By default it
    // will return 10 items.
    $product_snippets = $this->g2Api->getProductSnippets($product_id);

    $this->assertNotNull($product_snippets, $fail_message);

    // Check the response structure.
    $this->assertEquals(
      ['data', 'links', 'meta'],
      array_keys($product_snippets),
      $structure_mismatch
    );
  }

  /**
   * Test getting the list of sindicate reviews comments from G2.
   *
   * @dataProvider configDataProvider
   */
  public function testGetProductReviewsComments(string $product_id, string $fail_message, string $structure_mismatch) {
    // Try to get the list of product reviews comments without any filter. By
    // default it will return 10 items.
    $review_comments = $this->g2Api->getProductReviewsComments($product_id);

    $this->assertNotNull($review_comments, $fail_message);

    // Check the response structure.
    $this->assertEquals(
      ['data', 'links', 'meta'],
      array_keys($review_comments),
      $structure_mismatch
    );
  }

  /**
   * Test getting the review form for a particular product from G2.
   *
   * @dataProvider configDataProvider
   */
  public function testGetProductEmbeddedtReviewForm(string $product_id, string $fail_message, string $structure_mismatch) {
    // Try to get the review form.
    $review_form = $this->g2Api->getProductEmbeddedtReviewForm($product_id, 'g2sandbox@example.com');

    $this->assertNotNull($review_form, $fail_message);

    // Check the response structure.
    $this->assertEquals(
      ['data'],
      array_keys($review_form),
      $structure_mismatch
    );
  }

}
